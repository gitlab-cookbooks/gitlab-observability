include_controls 'netdata'

control 'PostgreSQL observability' do
  title 'Check if postgres.conf is configured for Netdata'

  case os.release.to_i
  when 16, 18
    describe package('python-psycopg2') do
      it { should be_installed }
    end
  when 20
    describe package('python3-psycopg2') do
      it { should be_installed }
    end
  end

  describe file('/opt/netdata/etc/netdata/python.d.conf') do
    its('owner')   { should eq 'netdata' }
    its('group')   { should eq 'netdata' }
    its('content') { should match(/enabled: yes/) }
  end

  describe file('/opt/netdata/etc/netdata/python.d/postgres.conf') do
    its('owner')   { should eq 'netdata' }
    its('group')   { should eq 'netdata' }
    its('content') { should match(/tcp:/) }
    its('content') { should match(/name: 'default'/) }
    its('content') { should match(/user: 'gitlab-superuser'/) }
    its('content') { should match(/password: 'superuser-password'/) }
    its('content') { should match(/database: 'gitlabhq_production'/) }
    its('content') { should match(/host: 'localhost'/) }
    its('content') { should match(/port: 5432/) }
  end
end
