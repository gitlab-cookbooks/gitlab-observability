default['gitlab-observability']['netdata']['collector']['postgresql']['user'] = 'gitlab-superuser'
default['gitlab-observability']['netdata']['collector']['postgresql']['password'] = 'postgres'
default['gitlab-observability']['netdata']['collector']['postgresql']['database'] = 'gitlabhq_production'
default['gitlab-observability']['netdata']['collector']['postgresql']['host'] = 'localhost'
default['gitlab-observability']['netdata']['collector']['postgresql']['port'] = 5432

default['gitlab-observability']['jmeter']['version'] = '5.4.1'
default['gitlab-observability']['jmeter']['url']['download'] = 'https://mirror.its.dal.ca/apache/jmeter/binaries/apache-jmeter-%<version>s.zip'
default['gitlab-observability']['jmeter']['url']['sha512sum'] = 'https://www.apache.org/dist/jmeter/binaries/apache-jmeter-%<version>s.zip.sha512'

default['gitlab-observability']['jmeter']['jdbc']['postgresql']['version'] = '42.2.23'
default['gitlab-observability']['jmeter']['jdbc']['postgresql']['url'] = 'https://jdbc.postgresql.org/download/postgresql-%<version>s.jar'

# Secrets management
default['gitlab-observability']['secrets']['backend'] = 'dummy'
default['gitlab-observability']['secrets']['path'] = 'secrets'
default['gitlab-observability']['secrets']['key'] = 'gitlab-patroni'
