# Cookbook:: gitlab-observability
# Recipe:: netdata_postgres
# License:: MIT
#
# Copyright:: 2021, GitLab Inc.

include_recipe '::netdata'

if node['gitlab-observability']['secrets']['backend'] != 'dummy'
  include_recipe '::populate_secrets'
end

# https://github.com/netdata/netdata/blob/597763dd0ae60dd89af211267a17644e00114537/collectors/python.d.plugin/postgres/README.md
package 'Install dependency: psycopg2' do
  case node['platform_version'].to_i
  when 16, 18
    package_name 'python-psycopg2'
  when 20
    package_name 'python3-psycopg2'
  end
end

file '/opt/netdata/etc/netdata/python.d.conf' do
  action  :create
  content 'enabled: yes'
  owner   'netdata'
  group   'netdata'
  mode    '0755'
  not_if { ::File.exist?('/opt/netdata/etc/netdata/python.d.conf') }
end

template '/opt/netdata/etc/netdata/python.d/postgres.conf' do
  source 'netdata/collectors/postgres.conf.erb'
  owner  'netdata'
  group  'netdata'
  mode   '0755'
  variables({
    postgres_user: node['gitlab-observability']['netdata']['collector']['postgresql']['user'],
    postgres_password: node['gitlab-observability']['netdata']['collector']['postgresql']['password'],
    postgres_database: node['gitlab-observability']['netdata']['collector']['postgresql']['database'],
    postgres_host: node['gitlab-observability']['netdata']['collector']['postgresql']['host'],
    postgres_port: node['gitlab-observability']['netdata']['collector']['postgresql']['port'],
  })
  notifies :restart, 'service[netdata]', :delayed
end
