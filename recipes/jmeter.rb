# Cookbook:: gitlab-observability
# Recipe:: netdata
# License:: MIT
#
# Copyright:: 2021, GitLab Inc.

jmeter_version = node['gitlab-observability']['jmeter']['version']
jmeter_postgres_jdbc_version = node['gitlab-observability']['jmeter']['jdbc']['postgresql']['version']

apt_update

package %w(unzip default-jre)

remote_file "#{Chef::Config[:file_cache_path]}/jmeter.zip.sha512" do
  source format(node['gitlab-observability']['jmeter']['url']['sha512sum'], version: jmeter_version)
  owner  'root'
  group  'root'
  mode   '0755'
  action :create
  not_if { ::File.exist?('/etc/jmeter') }
end

remote_file "#{Chef::Config[:file_cache_path]}/jmeter.zip" do
  source format(node['gitlab-observability']['jmeter']['url']['download'], version: jmeter_version)
  owner  'root'
  group  'root'
  mode   '0755'
  action :create
  not_if { ::File.exist?('/etc/jmeter') }
end

execute 'Extract jmeter.zip' do
  command "unzip #{Chef::Config[:file_cache_path]}/jmeter.zip -d /etc"
  not_if do
    require 'digest'
    file_checksum = Digest::SHA512.file("#{Chef::Config[:file_cache_path]}//jmeter.zip").hexdigest
    source_checksum = File.read("#{Chef::Config[:file_cache_path]}/jmeter.zip.sha512").split(' ')[0]
    if file_checksum != source_checksum
      raise "Downloaded JMeter checksum #{file_checksum} does not match expected checksum #{source_checksum}"
    end
    ::File.exist?('/etc/jmeter')
  end
end

execute "Move /etc/apache-jmeter-#{jmeter_version} to /etc/jmeter" do
  command "mv /etc/apache-jmeter-#{jmeter_version} /etc/jmeter"
  not_if { ::File.exist?('/etc/jmeter') }
end

link '/usr/sbin/jmeter' do
  to '/etc/jmeter/bin/jmeter'
  link_type :symbolic
  not_if { ::File.exist?('/usr/sbin/jmeter') }
end

remote_file '/etc/jmeter/lib/postgresql.jar' do
  source format(node['gitlab-observability']['jmeter']['jdbc']['postgresql']['url'], version: jmeter_postgres_jdbc_version)
  owner  'root'
  group  'root'
  mode   '0755'
  action :create
  not_if { ::File.exist?('/etc/jmeter/lib/postgresql.jar') }
end
