# Cookbook:: gitlab-observability
# Recipe:: populate_secrets
# License:: MIT
#
# Copyright:: 2021, GitLab Inc.

secrets_hash = node['gitlab-observability']['secrets']
secrets = get_secrets(secrets_hash['backend'], secrets_hash['path'], secrets_hash['key'])
node.default['gitlab-observability']['netdata']['collector']['postgresql']['password'] = secrets['gitlab-patroni']['patroni']['users']['superuser']['password']
