# Variables
BUNDLE_PATH ?= ./.bundle
KITCHEN_INSTANCE ?= ""

.PHONY: install_dependencies
install_dependencies:
	bundle install --path $(BUNDLE_PATH)

.PHONY: lint
lint:
	bundle exec cookstyle

.PHONY: lint_autofix
lint_autofix:
	bundle exec cookstyle --auto-correct-all

.PHONY: converge
converge:
	bundle exec kitchen converge "${KITCHEN_INSTANCE}"

.PHONY: verify
verify:
	bundle exec kitchen verify "${KITCHEN_INSTANCE}"

.PHONY: list
list:
	bundle exec kitchen list

.PHONY: test
test:
	bundle exec kitchen test "${KITCHEN_INSTANCE}" --destroy=always
