# Cookbook:: gitlab-observability
# Recipe:: netdata
# License:: MIT
#
# Copyright:: 2021, GitLab Inc.

# Avoid message "WARNING: Can't find jq command line tool. jq is required for netdata to retrieve container name"
package 'jq'

# https://learn.netdata.cloud/docs/agent/packaging/installer/methods/kickstart-64
remote_file "#{Chef::Config[:file_cache_path]}/kickstart-static64.sh" do
  source   'https://my-netdata.io/kickstart-static64.sh'
  owner    'root'
  group    'root'
  mode     '0755'
  action   :create
end

# The `--dont-start-it` argument is required to avoid pipeline failure when testing on Ubuntu 20.04.
execute 'Install Netdata' do
  command "yes | #{Chef::Config[:file_cache_path]}/kickstart-static64.sh --disable-telemetry --stable-channel --no-updates --dont-start-it"
  not_if { ::File.exist?('/etc/netdata') }
end

service 'netdata' do
  action :enable
end

service 'netdata' do
  action :start
  subscribes :restart, 'template[/etc/netdata/python.d/postgresql.conf]', :delayed
end
