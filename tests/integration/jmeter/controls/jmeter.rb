control 'JMeter' do
  title 'Check if application is configured'

  describe command('java -version') do
    its('stderr') { should match(/OpenJDK/) }
    its('exit_status') { should eq 0 }
  end

  describe file('/usr/sbin/jmeter') do
    it { should be_symlink }
    it { should_not be_directory }
    its('shallow_link_path') { should eq '/etc/jmeter/bin/jmeter' }
  end

  describe command('jmeter --version') do
    its('stdout') { should match(/5.4.1/) }
    its('exit_status') { should eq 0 }
  end

  describe file('/etc/jmeter/lib/postgresql.jar') do
    it { should exist }
    it { should_not be_directory }
    it { should be_file }
  end
end
