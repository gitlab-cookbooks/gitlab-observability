control 'Netdata' do
  title 'Check if package is installed and service is running'

  describe service('netdata') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

  # Extra verification if the test is running on Ubuntu 20.04 to help the debug in case of failure in the GitLab
  # pipeline
  if os.release.to_i == 20
    describe command('systemctl status netdata') do
      its('exit_status') { should eq 0 }
      its('stdout') { should match(/running/) }
    end
  end
end
