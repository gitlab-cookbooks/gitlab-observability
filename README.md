# GitLab Observability Chef Cookbook

Proof of Concept cookbook to install observability tools and use Docker for development and test.

## Requirements

- [Ruby](https://www.ruby-lang.org/en/documentation/installation/)
- [Make](https://www.google.com/search?q=install+make)
- [Docker](https://docs.docker.com/get-docker/)

## Install dependencies

The dependency management relies on [RubyGems](https://rubygems.org/) and can be installed by running the following command:
```
make install_dependencies
```

## Development

The development workflow was designed to use [Test Kitchen](https://github.com/test-kitchen/test-kitchen) and [Kitchen Dokken](https://github.com/test-kitchen/kitchen-dokken) — which leverages Docker containers — to simplify and speed up the feedback loop (write code, test it, repeat). Therefore, we wrapped the mostly common Test Kitchen commands with Make targets.

Test Kitchen `list`:
```
make list
```

Test Kitchen `converge`:
```
make converge KITCHEN_INSTANCE=<instance_name>
```

Test Kitchen `verify`:
```
make converge KITCHEN_INSTANCE=<instance_name>
```

## Tests

### Lint

Cookstyle was adopted as linter and can be used by running:
```
make lint
```
_Note_: If any issues are detected you can attempt to automatically solve them by running `make lint_autofix`.

### Configuration management

To run all the tests sequentially, please execute:
```
make test
```

To test a single Test Kitchen instance end-to-end, please execute:
```
make test KITCHEN_INSTANCE=<instance_name>
```

## Issue creation

Please create new issues in the [public infrastructure project](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/) and not in this private issue tracker.

## Project Workflow

**[CONTRIBUTING.md](CONTRIBUTING.md) is important, do not skip reading this.**
