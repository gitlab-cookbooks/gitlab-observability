name             'gitlab-observability'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Installs and configures monitoring tools for GitLab'
version          '1.0.10'
chef_version     '>= 12.1'
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab-observability/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab-observability'

supports 'ubuntu', '>= 16.04'

depends 'gitlab_secrets'
